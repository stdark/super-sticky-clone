﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteBackgroundController : MonoBehaviour
{
    [Header("Parameters")] 
    [SerializeField] private float speed = 1f;
    [Header("Cache")] 
    [SerializeField] private Transform cameraTransform;
    
    private GameManager _gameManager;

    private Material _material;

    private void Awake()
    {
        _material = GetComponent<Renderer>().material;
    }

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_gameManager.IsGameStarted) return;
        float newY = (cameraTransform.position.y) * Constants.GAME_SPEED * speed;
        _material.mainTextureOffset = new Vector2(_material.mainTextureOffset.x, newY);
    }
}