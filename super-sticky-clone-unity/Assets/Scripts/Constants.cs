﻿public static class Constants
{
    // Tags strings
    public const string LEFT_WALL_TAG = "LeftWall";
    public const string RIGHT_WALL_TAG = "RightWall";
    public const string UPPER_BORDER_TAG = "UpperBorder";
    public const string PLAYER_TAG = "Player";
    public const string TOXIC_TAG = "Toxic";
    public const string FLY_TAG = "Fly";
    
    // Animators strings
    public const string IS_JUMPING_PARAM = "isJumping";
    
    // Game Parameters
    public const float GAME_SPEED = 5f;

}
