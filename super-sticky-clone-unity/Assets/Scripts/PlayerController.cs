﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool _isOnRightWall = true;
    private bool _isOnWall = true;

    private GameManager _gameManager;
    
    private Rigidbody2D _rigidbody2D;
    private Transform _transform;
    private Animator _animator;
    private bool _canMove = true;
    private Vector3 _startPosition;

    private void Awake()
    {
        CacheInit();
    }
    
    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    private void CacheInit()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _transform = transform;
        _startPosition = _transform.position;
        
    }

    public void MovePlayer()
    {
        _isOnWall = false;
        _animator.SetBool(Constants.IS_JUMPING_PARAM, true);
        if (_isOnRightWall)
        {
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.AddForce(new Vector2(-10f,Constants.GAME_SPEED*1.6f), ForceMode2D.Impulse);
            _isOnRightWall = false;
        }
        else
        {
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.AddForce(new Vector2(10f,Constants.GAME_SPEED*1.6f),ForceMode2D.Impulse);
            _isOnRightWall = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        GameObject otherGameObject = other.gameObject;
        
        switch (otherGameObject.tag)
        {
            case Constants.LEFT_WALL_TAG:
                _isOnRightWall = false;
                _isOnWall = true;
                _transform.localScale = new Vector2(-1f,1f);
                break;
            case Constants.RIGHT_WALL_TAG:
                _isOnRightWall = true;
                _isOnWall = true;
                _transform.localScale = new Vector2(1f,1f);
                break;
        }
        _animator.SetBool(Constants.IS_JUMPING_PARAM, false);
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        GameObject otherGameObject = other.gameObject;
        
        switch (otherGameObject.tag)
        {
            case Constants.UPPER_BORDER_TAG:
                _canMove = false;
                break;
            case Constants.FLY_TAG:
                _gameManager.AddFlyCost();
                Destroy(otherGameObject);
                break;
        }
    }
    
    
    
    private void OnTriggerExit2D(Collider2D other)
    {
        GameObject otherGameObject = other.gameObject;
        
        switch (otherGameObject.tag)
        {
            case Constants.UPPER_BORDER_TAG:
                _canMove = true;
                break;
        }
    }

    private void LateUpdate()
    {
        if (!_gameManager.IsGameStarted)
        {
            _rigidbody2D.velocity = Vector2.zero;
            _transform.position = _startPosition;
            return;
        }
        if ((Input.GetKeyDown(KeyCode.Space)||Input.GetMouseButtonDown(0))&&_isOnWall&&_canMove)
        {
            MovePlayer();
        }
    }
}
