﻿using UnityEngine;

public class FlyGenerator : MonoBehaviour
{
    [SerializeField] private GameObject flyGameObject;
    [SerializeField] private float delayMin = 1f;
    [SerializeField] private float delayMax = 3f;

    [Header("Fly Init Borders")] 
    [SerializeField] private float xMin;
    [SerializeField] private float xMax;
    [SerializeField] private float yMin;
    [SerializeField] private float yMax;


    private GameManager _gameManager;

    private Transform _transform;

    private float _currentTimerState;
    private float _currentDelay;

    private void Awake()
    {
        _currentDelay = Random.Range(delayMin, delayMax);
        _transform = transform;
    }

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    private void Generate()
    {
        _currentTimerState = 0f;
        Vector3 position = _transform.position;
        Vector3 newFlyPosition = new Vector3(position.x + Random.Range(xMin, xMax),
            position.y + Random.Range(yMin, yMax), position.z);
        GameObject newFly = Instantiate(flyGameObject, newFlyPosition, Quaternion.identity);
        newFly.transform.parent = _transform;

        _currentDelay = Random.Range(delayMin, delayMax);
    }

    private void Update()
    {
        if (!_gameManager.IsGameStarted) return;
        _currentTimerState += Time.deltaTime;
        if (_currentTimerState >= _currentDelay)
        {
            Generate();
        }
    }
}