﻿using UnityEngine;

public class FollowingController : MonoBehaviour
{
    [SerializeField] private Transform targetTransform;
    [SerializeField] private float speed = 5f;
    
    private GameManager _gameManager;
    
    private Transform _transform;
    

    private void Awake()
    {
        _transform = transform;
    }

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }
    
    private void Update()
    {
        if (!_gameManager.IsGameStarted) return;
        Vector3 position = _transform.position;
        _transform.position = Vector3.Lerp(position, new Vector3(position.x,position.y + Constants.GAME_SPEED), 0.01f);
    }
}