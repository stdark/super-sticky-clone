﻿using UnityEngine;

public class ToxicParticleCollider : MonoBehaviour
{
    private GameManager _gameManager;

    private ParticleSystem _main;

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
        _main = GetComponent<ParticleSystem>();
    }

    private void Update()
    {
        if (!_gameManager.IsGameStarted || _main.isPlaying) return;
        _main.Play();
    }

    private void OnParticleCollision(GameObject other)
    {
        switch (other.tag)
        {
            case Constants.PLAYER_TAG:
                _gameManager.InitLooseState();
                break;
            default:
                Destroy(other.gameObject, 1.5f);
                break;
        }
    }
}