﻿using UnityEngine;

public static class PlayerPrefsController
{
    private const string SCORE_KEY = "PlayerScore";

    private const int DEFAULT_SCORE = 0;

    public static int Score => PlayerPrefs.HasKey(SCORE_KEY) ? PlayerPrefs.GetInt(SCORE_KEY) : DEFAULT_SCORE;

    public static void SaveHighScore(int score)
    {
        if (score > Score)
        {
            PlayerPrefs.SetInt(SCORE_KEY, score);
        }
        
    }
}
