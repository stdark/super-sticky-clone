﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private const int MENU_BUILD_INDEX = 0;
    private const int GAME_SCENE_BUILD_INDEX = 1;

    public void LoadGameSceneWrapper()
    {
        LoadGameScene();
    }

    public void LoadMainMenuWrapper()
    {
        LoadMainMenu();
    }

    public static void LoadMainMenu()
    {
        SceneManager.LoadScene(MENU_BUILD_INDEX);
    }

    public static void LoadGameScene()
    {
        SceneManager.LoadScene(GAME_SCENE_BUILD_INDEX);
    }
}