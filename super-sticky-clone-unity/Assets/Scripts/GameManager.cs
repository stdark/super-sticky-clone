﻿using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currentScoreText;
    [Header("Item Values")]
    [SerializeField] private int flyCost=1;
    
    
    
    private PlayerController _playerController;
    
    private bool _isGameStarted;
    private int _currentScore;

    public bool IsGameStarted => _isGameStarted;

    private void Start()
    {
        _playerController = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (!_isGameStarted && (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)))
        {
            _isGameStarted = true;
            _playerController.MovePlayer();
        }
    }

    public void InitLooseState()
    {
        PlayerPrefsController.SaveHighScore(_currentScore);
        SceneLoader.LoadMainMenu();
    }
    

    public void AddFlyCost()
    {
        AddScore(flyCost);
    }
    
    private void AddScore(int value)
    {
        _currentScore += value;
        UpdateCurrentScore();
    }

    private void UpdateCurrentScore()
    {
        currentScoreText.text = _currentScore.ToString();
    }
    
    
}
