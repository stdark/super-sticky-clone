﻿using UnityEngine;

public class Singleton <T> : MonoBehaviour where T : MonoBehaviour
{
    private void Awake()
    {
        int count = FindObjectsOfType<T>().Length;
        if (count > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
