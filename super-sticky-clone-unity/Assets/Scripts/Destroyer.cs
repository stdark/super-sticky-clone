﻿using UnityEngine;

public class Destroyer : MonoBehaviour
{
    private GameManager _gameManager;

    private void Start()
    {
        _gameManager = FindObjectOfType<GameManager>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.gameObject.tag)
        {
            case Constants.PLAYER_TAG:
                _gameManager.InitLooseState();
                break;
            default:
                Destroy(other.gameObject,1.5f);
                break;
        }
    }
}
